from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier 

from demo_code import *

import matplotlib.pyplot as plt
from IPython import display

plt.style.use('dark_background')


def experiment_1():
    
    # Create global clock, and a virtual user (walker)
    clock = Clock()
    walker = Walker(origin=np.array([0,0]), 
                    target=np.array([1,1]), 
                    speed=0.01, 
                    tstart=0, 
                    clock=clock)
    walker_ = Walker(origin=np.array([0,0]), 
                    target=np.array([1,1]), 
                    speed=0.01, 
                    tstart=100, 
                    clock=clock)


    # Create lights
    lights = []
    for x in np.linspace(0,1,11):
        lamp_position = np.ones(2) * x
        sensor = Sensor(walkers=[walker, walker_], 
                        pos=lamp_position,
                        radius=0.1)
        lights += [Lamp(clock=clock, 
                        sensor=sensor, 
                        com_module=CommunicationModule([]), 
                        T=20, 
                        lookback=10, 
                        predictive_model=LogisticRegression(C=1e5, fit_intercept=False, penalty='l1'))]
                        #predictive_model=RandomForestClassifier(n_jobs=2, random_state=0))]

    # Connect the lights
    R = 0.5
    for i,hi in enumerate(lights):
        for j,hj in enumerate(lights):
            if i==j: continue
            if norm(hi.sensor.pos - hj.sensor.pos) <= R:
                hi.com_module.other_lamps += [hj]


    # Enable smart operation
    [l.enable_smart_operation() for l in lights]

    
    ####################################################################################
    # Run simulation    
    fig = plt.figure(figsize=(5,5))       
    niter = 200
    clock.t = 0
    for i in range(1,niter+1):

        clock.time_pass(int(1))
        [l.main() for l in lights]

        if i%2 == 0:

            ax = plt.gca()
            plt.axis('off')
            plt.style.use('dark_background')


            [hl.draw(ax) for hl in lights]
            [w.draw(ax) for w in [walker, walker_]]

            display.display(plt.gcf())
            display.clear_output(wait=True)
            time.sleep(0.001)
            if i!=niter: plt.clf()

                
def experiment_2():
    
    # Create global clock, and a virtual user (walker)
    clock = Clock()
    walker = Walker(origin=np.array([0,0]), 
                    target=np.array([1,1]), 
                    speed=0.01, 
                    tstart=0, 
                    clock=clock)
    walker_ = Walker(origin=np.array([0,0]), 
                    target=np.array([1,1]), 
                    speed=0.01, 
                    tstart=100, 
                    clock=clock)


    # Create lights
    lights = []
    for x in np.linspace(0,1,11):
        lamp_position = np.ones(2) * x
        sensor = Sensor(walkers=[walker, walker_], 
                        pos=lamp_position,
                        radius=0.1)
        lights += [Lamp(clock=clock, 
                        sensor=sensor, 
                        com_module=CommunicationModule([]), 
                        T=20, 
                        lookback=10, 
                        predictive_model=LogisticRegression(C=1e5, fit_intercept=False, penalty='l1'))]
                        #predictive_model=RandomForestClassifier(n_jobs=2, random_state=0))]

    # Connect the lights
    R = 0.5
    for i,hi in enumerate(lights):
        for j,hj in enumerate(lights):
            if i==j: continue
            if norm(hi.sensor.pos - hj.sensor.pos) <= R:
                hi.com_module.other_lamps += [hj]


    # Enable smart operation
    [l.enable_smart_operation() for l in lights]

    ####################################################################################
    # Run simulation ones (to train the lamps)  
    fig = plt.figure(figsize=(5,5))       
    niter = 200
    clock.t = 0
    for i in range(1,niter+1):

        clock.time_pass(int(1))
        [l.main() for l in lights]
        
    ####################################################################################
    # Run simulation again, with smart lights
    
    # Train the lamps, using the data from the previous run smart operation
    [l.retrain_model() for l in lights]

    # Run simulation    
    fig = plt.figure(figsize=(5,5))       
    niter = 200
    clock.t = 0
    for i in range(1,niter+1):

        clock.time_pass(int(1))
        [l.main() for l in lights]

        if i%2 == 0:

            ax = plt.gca()
            plt.axis('off')
            plt.style.use('dark_background')


            [hl.draw(ax) for hl in lights]
            [w.draw(ax) for w in [walker, walker_]]

            display.display(plt.gcf())
            display.clear_output(wait=True)
            time.sleep(0.001)
            if i!=niter: plt.clf()


