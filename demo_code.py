
import numpy as np
import pandas as pd
import time

from scipy.linalg import norm

import matplotlib.pyplot as plt


class Lamp:
    
    def __init__(self, clock, sensor, com_module, T, lookback, predictive_model):
        
        # A timer
        self.clock = clock
        
        # The lamps's proximity sensor
        self.sensor = sensor
        
        # Handles to instances of neighbouring lamps 
        self.com_module = com_module
        
        # Is the map turned on? (this would be controlling a relay/dimmer)
        self.lights_on = 1
        
        # Time since any neighbour was last activated
        self.last_activation = -99999
        
        # If smart operation is Flase the lamp is always on.
        self.smart_operation = False
        
        # The time look-ahead: how long before the user appraoches do we want to turn on the light?
        self.T = T
        
        # How far back in history should the smartlamp's predictive model look for patterns.
        self.lookback = lookback
        
        # Storing all recent activity
        self.neighbour_recent_activity = []
        self.recent_own_state = []
        
        # Storign features and outcomes
        self.Xraw = []
        self.Yraw = []
        
        # The lamp's predictive model
        self.predictive_model = predictive_model
        self.istrained = False
        
        # How far the light illuminates (just for plotting in the demo)
        self.light_radius = 0.2
                    
    def enable_smart_operation(self):
        
        # Enable smart operation
        self.smart_operation = True
    
    def main(self):
        
        # Collect data
        self.sensor.update()
        own_sensor_state = self.sensor.state
        
        # Communicate (in deployment this would happen asychronously)
        self.com_module.update()
        neighbour_sensor_state = self.com_module.state
        
        # Update reccent logs 
        self.neighbour_recent_activity += [neighbour_sensor_state.copy()]
        self.recent_own_state += [own_sensor_state]
         
        # Store recent sensor measurements to local DB, if a sensor was recently activated.
        self.store_recent_activity() 

        # Predict future state
        user_expected = self.predict_sensor_state()
        
        # Determine if the light should be turned on or not
        if (own_sensor_state) or (not self.smart_operation) or (user_expected):
            self.lights_on = 1
        else:
            self.lights_on = 0


    def store_recent_activity(self):
        
        # Check is there was any activity at all reccently
        temp = np.array(
                self.neighbour_recent_activity[-self.lookback-self.T:-self.T]
            ).flatten()
        
        # Also check if we are far in the simulation enough..
        flag = len(self.neighbour_recent_activity[-self.lookback-self.T:-self.T]) == self.lookback
        
        # If so, store to DB...
        if np.sum(temp) != 0 and flag:
            
            # .. the reccent past..
            self.Xraw += [temp]
            
            # .. and whether our sensor was activate reccently
            self.Yraw += [np.max(self.recent_own_state[-self.T:])]  
         
    def retrain_model(self):
        # Do we have at least two classes to train on? If not skip training.
        if len(np.unique(np.array(self.Yraw)))>1:
            self.predictive_model.fit(X=np.array(self.Xraw), y=np.array(self.Yraw))
            self.istrained = True
       
    def predict_sensor_state(self):
        if self.istrained:
            X = np.array(self.neighbour_recent_activity[-self.lookback:]).flatten()
            prediction = self.predictive_model.predict(np.array([X]))
            return prediction
        else: 
            return False
    
    def draw(self, ax):
        ax.plot(self.sensor.pos[0], self.sensor.pos[1], 'wo')
        circ = plt.Circle(self.sensor.pos, 
                          self.light_radius, 
                          alpha=self.lights_on*0.8, 
                          color='white', 
                          clip_on=False)
        ax.add_artist(circ)


class Sensor:
    """Implements a basic proximity sensor. Keeps track of all 'walkers' in the demo, and turns on when someone is near."""
    def __init__(self, pos, walkers, radius):
        
        # Location in space
        self.pos = pos
        
        # Sensor radius
        self.radius = radius
        
        # Sensor state
        self.state = 0
        
        # Handles to all walker instances
        self.walkers = walkers
        
    def update(self):
        temp = [norm(w.get_position()-self.pos)<self.radius for w in self.walkers]
        if np.array(temp).any(): self.state = 1
        else:                    self.state = 0
        

class CommunicationModule:
    """Implements a basic com module, capable to informing us about the state of other lamps.
    Here, this happens synchronously (we ask all lamps for their sensor state).
    It deployment it would be asynchronous."""
    def __init__(self, other_lamps):
        
        self.other_lamps = other_lamps
        
        self.state = None
    
    def update(self):
        self.state = np.array([l.sensor.state for l in self.other_lamps])
    

class Clock:
    """ A rudimentary clock. It let's the smartlamp keep track of time. Time is discrete and integer. """
    def __init__(self):
        self.t = 0
        
    def time_pass(self, t):
        self.t += t
        

class Walker:
    """Simulates a user, moving around in space"""
    def __init__(self, origin, target, speed, tstart, clock):
        self.speed    = speed
        self.origin   = origin
        self.target   = target
        self.tstart  = tstart
        self.clock  = clock
                
    def get_position(self):
        temp = np.clip(self.origin + (self.target - self.origin) * self.speed * self.clock.t,
            self.origin, self.target)
        return temp
        
    def draw(self, ax):

        pos = self.get_position()
        ax.plot(pos[0], pos[1], 'rx')
        