# Smart lamps for Singapore
### <Reducing energy waste through distributed artificial intelligence

This repository includes:

 1. a 'big picture' summary of an innovative smart light product (open and read the SolutionOverview notebook)

 2. a demo of the proposed solution (open and execute the cells in the demo_code notebook).

The two .py files include code of a fully functional, modular simulation of the proposed solution.